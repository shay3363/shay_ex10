#include "Customer.h"
Customer::Customer(string name)
{
	this->_name = name;
	_items = set<Item>();
}
Customer::Customer()
{
	this->_name = "";
}
Customer::~Customer()
{

}
double Customer::totalSum() const
{
	double sum = 0;
	for (set<Item>::iterator i = this->_items.begin(); i != this->_items.end(); i++)
	{
		sum += i->totalPrice();
	}
	return sum;
}
void Customer::addItem(Item newItem)
{
	this->_items.insert(newItem);
}
void Customer::removeItem(Item myItem)
{
	this->_items.erase(myItem);
}
set<Item> Customer::getItems()
{
	return this->_items;
}
string Customer::getName()
{
	return this->_name;
}
void Customer::setName(string name)
{
	this->_name = name;
}
void Customer::setItems(set<Item> items)
{
	this->_items = items;
}