#include "Item.h"
Item::Item(string name, string serialNumber, double price)
{ 
	this->_count = 1;
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = price;
}
Item::Item()
{
}
Item::~Item()
{

}
double Item::totalPrice() const
{
	return this->_count*this->_unitPrice;
}
bool Item::operator <(const Item& other) const
{
	bool flag = false;
	if (this->_serialNumber < other._serialNumber)
	{
		flag = true;
	}
	return flag;
}
bool Item::operator >(const Item& other) const
{
	bool flag = false;
	if (this->_serialNumber > other._serialNumber)
	{
		flag = true;
	}
	return flag;
}
bool Item::operator ==(const Item& other) const
{
	bool flag = false;
	if (this->_serialNumber == other._serialNumber)
	{
		flag = true;
	}
	return flag;
}
string Item::getName() const
{
	return this->_name;
}
string Item::getSerialNumber() const
{
	return this->_serialNumber;
}
int Item::getCount() const
{
	return this->_count;
}
double Item::getUnitPrice() const
{
	return this->_unitPrice;
}
void Item::setName(string name)
{
	this->_name = name;
}
void Item::setSerialNumber(string serialNumber)
{
	this->_serialNumber = serialNumber;
}
void Item::setCount(int count)
{
	this->_count = count;
}
void Item::setUnitPrice(double unitPrice)
{
	this->_unitPrice = unitPrice;
}