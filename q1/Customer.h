#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item newItem);//add item to the set
	void removeItem(Item myItem);//remove item from the set
	void setName(string name);
	void setItems(set<Item> items);
	string getName();
	set<Item> getItems();
private:
	string _name;
	set<Item> _items;


};